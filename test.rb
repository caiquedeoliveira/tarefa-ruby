require 'json'
file = File.read('./pokemons.json')

#puts JSON.parse(file)["count"] #1126

# TODOS OS NOMES DOS POKEMONS
allnames = []

res = JSON.parse(file)["results"]
res.each do |res|
    allnames.append(res["name"])
end

# NOMES DIFERENTES E REPETIDOS
diff_names = []
rep_names = []
allnames.each do |allnames|
    el = allnames.index("-")
    if el
        rep_names.append(allnames.slice(0, el))
    else
        diff_names.append(allnames) 
    end  
end

# FILTRANDO ARRAY DE NOMES REPETIDOS
rep_names.each do |rep_names|
    exist = diff_names.include? rep_names
    if exist == false
        diff_names.append(rep_names)
    end
end

puts diff_names # ARRAY COM TODOS OS NOMES DIFERENTES
puts diff_names.count #892

# FREQUENCIA DE NOMES
frequencia = rep_names.tally
puts frequencia.sort_by {|item| item[1]}


# FREQUENCIA DE LETRAS INICIAIS
diff_names.each do |nome|
    puts "#{nome} #{diff_names.count(nome)}"
end

